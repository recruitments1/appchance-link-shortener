from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from link_shortener.users.api.views import UserViewSet
from link_shortener.links.api.views import LinkViewSet


if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("links", LinkViewSet, 'links')

app_name = "api"
urlpatterns = router.urls
