import pytest
from rest_framework.test import APIRequestFactory

from link_shortener.links.tests.factories import api_request_factory
from link_shortener.users.models import User
from link_shortener.users.tests.factories import UserFactory


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def user() -> User:
    return UserFactory()


@pytest.fixture
def api_rf() -> APIRequestFactory:
    return api_request_factory()
