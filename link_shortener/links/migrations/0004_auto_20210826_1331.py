# Generated by Django 3.1.13 on 2021-08-26 13:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('links', '0003_auto_20210826_1319'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linkauthormeta',
            name='browser_family',
            field=models.CharField(max_length=255, null=True, verbose_name='Browser family'),
        ),
        migrations.AlterField(
            model_name='linkauthormeta',
            name='browser_version',
            field=models.CharField(max_length=255, null=True, verbose_name='Browser version'),
        ),
        migrations.AlterField(
            model_name='linkauthormeta',
            name='device_brand',
            field=models.CharField(max_length=255, null=True, verbose_name='Device brand'),
        ),
        migrations.AlterField(
            model_name='linkauthormeta',
            name='device_family',
            field=models.CharField(max_length=255, null=True, verbose_name='Device family'),
        ),
        migrations.AlterField(
            model_name='linkauthormeta',
            name='device_model',
            field=models.CharField(max_length=255, null=True, verbose_name='Device model'),
        ),
        migrations.AlterField(
            model_name='linkauthormeta',
            name='os_family',
            field=models.CharField(max_length=255, null=True, verbose_name='OS family'),
        ),
        migrations.AlterField(
            model_name='linkauthormeta',
            name='os_version',
            field=models.CharField(max_length=255, null=True, verbose_name='OS version'),
        ),
        migrations.AlterField(
            model_name='linkauthormeta',
            name='raw_user_agent',
            field=models.CharField(max_length=255, null=True, verbose_name='Raw user agent informations'),
        ),
    ]
