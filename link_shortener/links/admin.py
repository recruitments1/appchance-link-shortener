from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Link, LinkAuthorMeta


class AuthorMetaInline(admin.StackedInline):
    model = LinkAuthorMeta


@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    search_fields = ["original"]
    inlines = [AuthorMetaInline]
