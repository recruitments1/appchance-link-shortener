from random import choice
from string import ascii_letters, digits

from django.conf import settings
from django.db.models import CharField, Model, URLField, OneToOneField, CASCADE
from django.db.models.fields import IntegerField
from django.utils.translation import gettext_lazy as _

AVAIABLE_CHARS = ascii_letters + digits

class Link(Model):
    """Stored links."""

    original = URLField(_("Original link"), max_length=2048)
    identifier = CharField(_("Link identifier"), max_length=255, unique=True)
    views_number = IntegerField(_("Number of link views"), default=0, blank=True)

    def __str__(self) -> str:
        return self.original

    def increment_views_number(self) -> None:
        self.views_number += 1
        self.save()

    @staticmethod
    def get_identifier(chars: str = AVAIABLE_CHARS, size: int = getattr(settings, "MAXIMUM_URL_CHARS", 7)) -> str:
        """
        Creates a random string with the predetermined size
        """

        return "".join(
            [choice(chars) for _ in range(size)]
        )


class LinkAuthorMeta(Model):
    link = OneToOneField(Link, verbose_name=_("Link"), on_delete=CASCADE, related_name='author_meta')
    ip = CharField(_("Author IP address"), max_length=15, null=True)
    raw_user_agent = CharField(_("Raw user agent informations"), max_length=255, null=True)
    browser_family = CharField(_("Browser family"), max_length=255, null=True)
    browser_version = CharField(_("Browser version"), max_length=255, null=True)
    os_family = CharField(_("OS family"), max_length=255, null=True)
    os_version = CharField(_("OS version"), max_length=255, null=True)
    device_family = CharField(_("Device family"), max_length=255, null=True)
    device_brand = CharField(_("Device brand"), max_length=255, null=True)
    device_model = CharField(_("Device model"), max_length=255, null=True)

    def __str__(self) -> str:
        return self.link.original
