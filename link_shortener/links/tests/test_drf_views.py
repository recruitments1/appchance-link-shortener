import pytest
from rest_framework.test import APIRequestFactory
from link_shortener.links.tests.factories import api_request_factory
from rest_framework.response import Response
from django.urls import reverse
from link_shortener.links.api.views import LinkViewSet
from link_shortener.links.models import Link, LinkAuthorMeta
from unittest.mock import patch, Mock

pytestmark = pytest.mark.django_db


class TestLinkViewSet:
    def send_create_shortened_link_request(self, original_link: str, extra: dict = {}) -> Response:
        api_rf = api_request_factory()
        create_view = LinkViewSet.as_view({'post': 'create'})
        url = reverse('api:links-list')
        data = {
            'original': original_link
        }

        request = api_rf.post(url, data)
        request_meta = extra.get('request_meta', {})

        if request_meta and isinstance(request_meta, dict):
            for key, value in request_meta.items():
                request.META[key] = value

        return create_view(request)

    @pytest.mark.parametrize('original_link', [
        'http://www.google.com',
        'https://www.google.com',
        'http://www.google.com/',
        'https://www.google.com/',
        'http://google.com/',
        'https://google.com/',
        'https://www.google.com/search?q=test+this+url&sxsrf=' \
            'ALeKk02JW8CQVT4Ln6DcSpC345oygVzbkg%3A1629974316333&sou' \
            'rce=hp&ei=LG8nYbn1EJ6vqtsP2fKXmA8&iflsig=AINFCbYAAAAAY' \
            'Sd9PB5iUOLK0APc1Xm-1SH27KzWUMJd&oq=test+this+url&gs_' \
            'lcp=Cgdnd3Mtd2l6EAMyBQgAEIAEMgUIABDLATIGCAAQFhAeMgYIAB'
    ])
    def test_create_valid_links(self, original_link: str) -> None:
        response = self.send_create_shortened_link_request(original_link)

        assert response.status_code == 201

    @patch('django.http.HttpRequest.build_absolute_uri', Mock(return_value='http://testserver/api/links/'))
    @pytest.mark.parametrize('original_link', [
        'http://testserver/api/links/H7D6AS/',
        'https://testserver/api/links/H7D6AS/',
        'http://www.testserver/api/links/H7D6AS/',
        'https://www.testserver/api/links/H7D6AS/',
    ])
    def test_prevent_self_reference_links(self, original_link: str, api_rf: APIRequestFactory) -> None:
        response = self.send_create_shortened_link_request(original_link)

        assert response.status_code == 400

    def test_if_original_link_already_exist_return_created(self, api_rf: APIRequestFactory) -> None:
        original_link = 'http://google.com'
        response = self.send_create_shortened_link_request(original_link)
        second_response = self.send_create_shortened_link_request(original_link)

        assert second_response.data['generated_link'] == response.data['generated_link']
        assert Link.objects.all().count() == 1

    def test_views_counter(self, api_rf: APIRequestFactory) -> None:
        link = Link.objects.create(
            original='http://www.google.com',
            identifier='ZAQWSX'
        )

        url = reverse('api:links-detail', kwargs={'identifier': link.identifier})
        request = api_rf.get(url)
        hits_number = 10
        retrieve_view = LinkViewSet.as_view({'get': 'retrieve'}, detail=True)

        for i in range(hits_number):
            response = retrieve_view(request, identifier=link.identifier)

        link.refresh_from_db()

        assert link.views_number == hits_number

    def test_author_user_agent_informations(self) -> None:
        user_agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.3'
        response = self.send_create_shortened_link_request('http://google.com', extra={
            'request_meta': {
                'HTTP_USER_AGENT': user_agent,
            }
        })

        assert LinkAuthorMeta.objects.all().count() == 1

        author_meta = LinkAuthorMeta.objects.all().first()

        assert author_meta.raw_user_agent == user_agent
        assert author_meta.browser_family == 'Firefox'
        assert author_meta.browser_version == '47.3'
        assert author_meta.os_family == 'Windows'
        assert author_meta.os_version == '7'

    def test_author_ip_address(self, api_rf: APIRequestFactory) -> None:
        author_ip = '192.168.8.112'
        response = self.send_create_shortened_link_request('http://google.com', extra={
            'request_meta': {
                'REMOTE_ADDR': author_ip,
            }
        })

        assert LinkAuthorMeta.objects.all().count() == 1

        author_meta = LinkAuthorMeta.objects.all().first()

        assert author_meta.ip == author_ip
