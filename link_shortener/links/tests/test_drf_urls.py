import pytest
from django.urls import resolve, reverse

pytestmark = pytest.mark.django_db

def test_viewset_list_url() -> None:
    assert reverse("api:links-list") == "/api/links/"
    assert resolve("/api/links/").view_name == "api:links-list"
