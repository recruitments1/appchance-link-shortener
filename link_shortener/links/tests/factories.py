from rest_framework.test import APIRequestFactory


def api_request_factory() -> APIRequestFactory:
    return APIRequestFactory()
