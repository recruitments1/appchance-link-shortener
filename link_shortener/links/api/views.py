from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from ipware import get_client_ip
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet
from user_agents import parse

from link_shortener.links.models import Link, LinkAuthorMeta

from ..models import Link
from .serializers import LinkSerializer


class LinkViewSet(ModelViewSet):
    permission_classes = [AllowAny]
    serializer_class = LinkSerializer
    queryset = Link.objects.all()
    lookup_field = "identifier"

    def retrieve(self, request, *args, **kwargs) -> HttpResponseRedirect:
        instance = self.get_object()
        instance.increment_views_number()
        return redirect(instance.original)

    def perform_create(self, serializer: LinkSerializer) -> None:
        link = serializer.save()
        ip, is_routable = get_client_ip(self.request)
        http_user_agent = self.request.META.get('HTTP_USER_AGENT', '')
        user_agent = parse(http_user_agent)

        if not hasattr(link, 'author_meta'):
            LinkAuthorMeta.objects.create(
                link=link,
                ip=ip,
                raw_user_agent=http_user_agent,
                browser_family=user_agent.browser.family,
                browser_version=user_agent.browser.version_string,
                os_family=user_agent.os.family,
                os_version=user_agent.os.version_string,
                device_family=user_agent.device.family,
                device_brand=user_agent.device.brand,
                device_model=user_agent.device.model,
            )
