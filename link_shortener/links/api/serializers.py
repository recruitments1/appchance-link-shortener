from typing import OrderedDict
from django.urls import reverse
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from link_shortener.links.models import Link

from ..models import Link


class LinkSerializer(serializers.ModelSerializer):
    generated_link = serializers.SerializerMethodField()
    self_reference_error = ValidationError('Invalid url. You cannot shorten links to the same page.')

    class Meta:
        model = Link
        fields = ["original", "generated_link"]

    def get_generated_link(self, instance: Link) -> str:
        url = reverse('api:links-detail', args=(instance.identifier, ))
        return self.context['request'].build_absolute_uri(url)

    def validate(self, attrs: OrderedDict) -> OrderedDict:
        uri = self.context['request'].build_absolute_uri('')
        uri_without_protocol = ''.join(uri.split('://')[1:])
        original_link_without_protocol = attrs.get('original').split('://')[1]

        if original_link_without_protocol.startswith('www.'):
            original_link_without_protocol = original_link_without_protocol[4:]

        if original_link_without_protocol.startswith(uri_without_protocol):
            raise self.self_reference_error

        return attrs

    def create(self, validated_data: dict) -> Link:
        original = validated_data.get('original')

        try:
            link = Link.objects.get(original=original)
        except Link.DoesNotExist:
            validated_data['identifier'] = Link.get_identifier()
            link = Link.objects.create(**validated_data)

        return link
